# tabela

### Simple app that shows CSV content in table.

*This app is made only for university purposes. Tabela shows content from CSV file in table. You can also add new rows and save table in CSV or XML.*

## Install on Linux


[![Get it from the Snap Store](https://snapcraft.io/static/images/badges/en/snap-store-white.svg)](https://snapcraft.io/tabela)



