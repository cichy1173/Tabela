from PySide6.QtCore import Qt, QAbstractTableModel
from PySide6.QtGui import QColor

class MyTableModel(QAbstractTableModel):
    def __init__(self, data, headers, parent=None):
        super().__init__(parent)
        self._data = data
        self._headers = headers
        self._adjust_data()

    def _adjust_data(self):
        max_len = max(len(row) for row in self._data)
        for row in self._data:
            if len(row) < max_len:
                row.extend([None] * (max_len - len(row)))

    def rowCount(self, parent=None):
        return len(self._data)

    def columnCount(self, parent=None):
        if self._data:
            return len(self._data[0])
        return 0

    def data(self, index, role=Qt.DisplayRole):
        if role == Qt.DisplayRole:
            row = index.row()
            col = index.column()
            if row < len(self._data) and col < len(self._data[row]):
                return str(self._data[row][col])
        elif role == Qt.BackgroundRole:
            row = index.row()
            col = index.column()
            if not self._data[row][col]:
                return QColor("red")
        return None

    def headerData(self, section, orientation, role=Qt.DisplayRole):
        if role == Qt.DisplayRole and orientation == Qt.Horizontal:
            return str(self._headers[section])
        return None
