import csv
import xml.dom.minidom as minidom

def copyDataFromCSV(filePath):
    with open(filePath, "r") as file:
        data = list(csv.reader(file, delimiter=","))

    return data


def savingFileWithCsvFormat(fileName, rows):

    with open(fileName, 'w') as file:
      
        write = csv.writer(file, delimiter=",")
        
        write.writerows(rows)


def saveFileAsXML(data, xml_file_path, head):

    doc = minidom.Document()
    root = doc.createElement("data")
    doc.appendChild(root)

    for row in data:
        elem = doc.createElement("row")
        for i in range(len(head)):
            node = doc.createElement(head[i])
            node.appendChild(doc.createTextNode(str(row[i])))  # Konwertujemy na str
            elem.appendChild(node)
        
        root.appendChild(elem)

    xml_string = doc.toprettyxml(indent="    ", encoding="utf-8")
    with open(xml_file_path, "wb") as xml_file:
        xml_file.write(xml_string)
