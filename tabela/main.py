from PySide6.QtWidgets import QApplication
from tabela.mainwindow import MainWindow
import sys
# import subprocess

def main():
    app = QApplication(sys.argv)
    window = MainWindow(app)
    window.show()
    sys.exit(app.exec())

if __name__ == '__main__':
    main()