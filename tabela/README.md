# Laboratorium 3 z Integracji systemów

Application project for University purposes.

## Installation

This app uses Python and Qt6 libraries. PySide6 is required.

```bash
pip install pyside6
```

## Usage

```bash
python3 main.py
```
## What's new in Laboratorium 3?

- Reading from XML
- Saving to XML
- Open regular CSV file (opens csv file without deleting last empty element)